(() => {
    'use strict'
  
    const forms = document.querySelectorAll('.needs-validation')
  
    Array.from(forms).forEach(form => {
      form.addEventListener('submit', event => {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
  
        form.classList.add('was-validated')
      }, false)
    })
})();

//JSON MONTH
var month = [       
    {
        "month": "January",
        "date": "01"
    },
    {
        "month": "February",
        "date": "02"
    },
    {
        "month": "March",
        "date": "03"
    },
    {
        "month": "April",
        "date": "04"
    },
    {
        "month": "May",
        "date": "05"
    },
    {
        "month": "June",
        "date": "06"
    },
    {
        "month": "July",
        "date": "07"
    },
    {
        "month": "August",
        "date": "08"
    },
    {
        "month": "September",
        "date": "09"
    },
    {
        "month": "October",
        "date": "10"
    },
    {
        "month": "November",
        "date": "11"
    },
    {
        "month": "December",
        "date": "12"
    }
];

var json_months = JSON.parse(JSON.stringify(month));
jQuery.each(json_months , function(k, item) {
    jQuery('#creditCardMonth')
        .append(jQuery("<option></option>")
        .attr("value", item.date)
        .text(item.date));
});

//JSON MONTH
var year = [       
    {
        "year": "2022"
    },
    {
        "year": "2023"
    },
    {
        "year": "2024"
    },
    {
        "year": "2025"
    },
    {
        "year": "2026"
    },
    {
        "year": "2027"
    },
    {
        "year": "2028"
    },
    {
        "year": "2029"
    },
    {
        "year": "2030"
    }
];

var json_year = JSON.parse(JSON.stringify(year));
jQuery.each(json_year , function(k, item) {
jQuery('#creditCardYear')
        .append(jQuery("<option></option>")
        .attr("value", item.year)
        .text(item.year));
});

//Functions
function limitMaxLength(field, maxChar){
    $(field).attr('maxlength', maxChar);
}

function checkDateCard(){
    var minMonth = new Date().getMonth() + 1;
    var minYear = new Date().getFullYear();
    var month = parseInt(jQuery('#creditCardMonth').val(), 10);
    var year = parseInt(jQuery('#creditCardYear').val(), 10);

    if (!month || !year || year < minYear || (year <= minYear && month <= minMonth)){
        jQuery('#invalid-feedback-card-date').html("Your Credit Card Expiration date is invalid.").addClass('d-block');
        jQuery('.btn-primary').prop("disabled", true);
    } else {
        jQuery('#invalid-feedback-card-date').removeClass('d-block');
        jQuery('.btn-primary').prop("disabled", false);
    }

}

//Card name validation
jQuery('#creditCardName').bind('keyup blur',function(){ 
    var letters = jQuery(this);
    letters.val(letters.val().replace(/[^A-Za-z\s]/g,'') );

});

//Card number validation

jQuery('#creditCardNumber').bind('keyup blur',function(){ 

    var number = jQuery(this);
    number.val(number.val().replace(/[^0-9]/g,'').replace(/(.{4})/g, '$1 ').trim());

    $numberVal = jQuery(this);

    if ($numberVal.val().length === 19) {
        jQuery('#invalid-feedback-card-number').removeClass('d-block');
    } else if($numberVal.val().length === 0) {
        jQuery('#invalid-feedback-card-number').html("This field can't be empty.").addClass('d-block');
    } else {
        jQuery('#invalid-feedback-card-number').html('Credit card number is invalid.').addClass('d-block');
    }

    limitMaxLength($numberVal, 19);
});


//Card number validation

jQuery('#creditCardCVV').bind('keyup blur',function(){ 

    var cvv = jQuery(this);
    cvv.val(cvv.val().replace(/[^0-9]/g,''));

    $cvvVal = jQuery(this);

    if ($cvvVal.val().length === 3 || $cvvVal.val().length === 4) {
        jQuery('#invalid-feedback-card-cvv').removeClass('d-block');
    } else if($cvvVal.val().length === 0) {
        jQuery('#invalid-feedback-card-cvv').html("This field can't be empty.").addClass('d-block');
    } else {
        jQuery('#invalid-feedback-card-cvv').html('Credit card CVV is invalid.').addClass('d-block');
    }

    limitMaxLength($cvvVal, 4);
});

jQuery('.card-date').on('change',function(){ 
    checkDateCard();
});
